#ifndef CAN_MSG_VALIDATION_H__
#define CAN_MSG_VALIDATION_H__

#include <stdbool.h>
#include <stdint.h>
#include "can.h"
#ifdef __cplusplus
extern "C" {
#endif

bool can_msg_validate(can_msg_t can_msg);


#ifdef __cplusplus
}
#endif
#endif
