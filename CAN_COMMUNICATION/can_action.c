#include <stdint.h>
#include <stdbool.h>
#include "switch_wrapper.h"
#include "can_msg_validation.h"
#include "can.h"

bool can_transmit(can_t can_bus){
    can_msg_t can_msg;
    can_msg.msg_id = 0x7c;
    can_msg.frame_fields.data_len = 1;
    can_msg.frame_fields.is_29bit = 0;
    can_msg.frame_fields.is_rtr = 0;
    can_msg.data.qword = 0x0;
    if (switch_read(1)){
        can_msg.data.bytes[0] = 0xAA;
    }
    else {
        can_msg.data.bytes[0] = 0x00;
    }

    if (can_msg_validate(can_msg)){
        CAN_tx(can_bus, &can_msg, 0);
        return true;
    }
    else {
        return false;
    }
}

bool can_check_and_reset(can_t can_bus){
    if(CAN_is_bus_off(can_bus)){
        CAN_reset_bus(can_bus);
        return true;
    }
    else {
        return false;
    }

}


